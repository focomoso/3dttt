'use strict';
// Todo:
// - snap to closet axis via arrows
// - move geo and meshes into new Board object. Game is just for the game logic


/** ttt3d
* 3d tic tac toe using threejs (and possibly firebase, but no angular)
* @author James H. Kelly / http://focomoso.com
*/



var scene, camera, renderer;
var renderW, renderH, camAspect;
var controls;
var renderId;

var raycaster, 
	mouse = new THREE.Vector2();

var camDist = 80;

var game, board;

var boardSlots = 3;


/**
* Initialize the game, board and threejs elements
*/
function init( ){

	// scene
	scene = new THREE.Scene(); 

	// game and board
	game = new Game( boardSlots );
	board = new Board( scene , game );

	// cam
	camera = new THREE.PerspectiveCamera( 
		75, 
		camAspect, 
		0.1, 
		1000 
	); 
	camera.position.z = camDist; 

	// renderer
	renderer = new THREE.WebGLRenderer({antialias: true}); 
	renderer.setSize( renderW, renderH ); 
	renderer.setClearColor(0xffffff,1);

	// interaction
	raycaster = new THREE.Raycaster();

	// lights
	var pointLight = new THREE.PointLight(0xFFFFFF,1,0);
	pointLight.position.set(-100,150,200);
	scene.add(pointLight);

	pointLight = new THREE.PointLight(0xFFFFFF,1,0);
	pointLight.position.set(100,-150,-200);
	scene.add(pointLight);

	var ambientLight = new THREE.AmbientLight(0x404040);
	scene.add( ambientLight );

	// add the trackball controller 
	controls = new THREE.TrackballControls( camera );
	controls.rotateSpeed = 0.5;
	controls.noZoom = true;

	document.addEventListener( 'mousedown', mouseDown, false );
	document.addEventListener( 'mousemove', mouseMove, false );
	document.addEventListener( 'mouseup', mouseUp, false );
	document.addEventListener( 'keydown', keyDown, false);
	window.addEventListener( 'resize', windowResize, false );
	document.body.appendChild( renderer.domElement ); 	
} // init

// resetting

function changeBoardSize(sel) {
	window.location.replace(window.location.origin + window.location.pathname + "?slots=" + sel.value);
}


/**
* set the dimentions, initialize and start the renderer when the scene loads
*/
window.onload = function(){

	// parse params
	var urlParams = {};
  var match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
      query  = window.location.search.substring(1);

  while (match = search.exec(query))
    urlParams[decode(match[1])] = decode(match[2]);

  if ( urlParams && urlParams.slots ) {
	  boardSlots = urlParams.slots;
	  if (boardSlots > 9)
	  	boardSlots = 9;
	  document.getElementById("board-size").value = boardSlots;
  }

	setDims();
	init();
	render();
};

/**
* resize the view when the window size changes
*/
function windowResize() {
	setDims();
	
	camera.aspect = camAspect;
	camera.updateProjectionMatrix();
	renderer.setSize( renderW, renderH );	
}

/**
* handle user events
*/

function getMouseSlot(mx, my) {
	var vector = new THREE.Vector3( mx, my, 1 ).unproject( camera );
	raycaster.set( camera.position, vector.sub( camera.position ).normalize() );
	var intersects = raycaster.intersectObjects( scene.children );

	if (intersects.length>0 && intersects[0].object.name.length > 0) {
		var vec = new THREE.Vector3();
		vec.x = Number(intersects[0].object.name[0]);
		vec.y = Number(intersects[0].object.name[2]);
		vec.z = Number(intersects[0].object.name[4]);
		return {
						mesh: intersects[0].object,
						slot: vec
		};
	}
	return undefined;
}

var dragging = false;

function mouseDown(e) {
	dragging = false;
}

function mouseMove(e) {
	//e.preventDefault();

	// get mouse position
	mouse.x = ( e.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;

	// clear all rollover mats
	var b = board.board;
	for (var x=0 ; x<b.length ; x++ ) {
		for (var y=0 ; y<b[x].length ; y++ ) {
			for (var z=0 ; z<b[y].length ; z++) {
				var obj = b[x][y][z];
				if (obj.interMesh) {
					obj.interMesh.material.opacity = 0.0;
				}
			} //z
		} //y
	} //x

	// get mouse object (what we're rolling over)
	var mo = getMouseSlot(mouse.x, mouse.y);

	// and set it visible
	if (mo) {
		var slot = mo.slot;
		if (!game.board[slot.x][slot.y][slot.z]) {
			mo.mesh.material.opacity = 0.3;
		}
	}
	
	dragging = true;
}

function mouseUp(e) {
	if (!dragging) {
		// get the mouse object we've clicked
		var mo = getMouseSlot(mouse.x, mouse.y);

		if (mo) {
			mo.mesh.material.opacity = 0.0;
			var slot = mo.slot;
			if (!game.board[slot.x][slot.y][slot.z]) {
				game.playSlot(slot.x,slot.y,slot.z);
				board.update();
			}
		}
	}
	dragging = false;
}

/**
* three js render function
*/
function render() { 
	renderId = requestAnimationFrame( render );
 	controls.update();
	var b = board.board;
	for (var x=0 ; x<b.length ; x++ ) {
		for (var y=0 ; y<b[x].length ; y++ ) {
			for (var z=0 ; z<b[y].length ; z++) {
				var obj = b[x][y][z];
				if (obj.token) {
					obj.token.rotation.x += obj.spin;
					obj.token.rotation.y += obj.spin;
				}
			} //z
		} //y
	} //x

	renderer.render(scene, camera); 
}

/**
*	sets the dimensions for the renderer and aspect for the cam
*/ 
function setDims() {
	renderW = window.innerWidth;
	renderH = window.innerHeight;
	camAspect = renderW/renderH;
}

// utils
var logVec =  function(vec, name) {
	name = name || "";

	console.log(name + " (" + 
		vec.x.toString().substring(0,5) + 
		", " + 
		vec.y.toString().substring(0,5) + 
		", " + 
		vec.z.toString().substring(0,5) + 
		")");
};


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

var nextSlot = 0,
		lastSlot = 0;	// the 6 possible viewing spots
function keyDown(e) {
	// use arrow keys to rotate view
	// later: animate transition
	
	if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
		// 				left:37 						 up:38 					 right:39  					 down:40

		var v = new THREE.Vector3().copy(camera.position);
		logVec(v.normalize(),"curr pos");

		//controls.rotateTo(new THREE.Vector3(1,0,0),new THREE.Vector3(0,1,0));
/*
		// simple look up for where to go given were we were
		var lut = [
// currSlot  0  1  2  3  4  5      key:
						[1, 2, 3, 0, 1, 1],	// left
						[4, 4, 4, 4, 2, 0], // up
						[3, 0, 1, 2, 3, 3],	// right
						[5, 5, 5, 5, 0, 2]	// down
		];	

		
		nextSlot = lut[e.keyCode-37][lastSlot];

		var cp = camera.position;
		switch (nextSlot) {
			case 0:
				cp.x = 0;
				cp.y = 0;
				cp.z = camDist;
				camera.up = new THREE.Vector3(0,1,0);
				break;
			case 1:
				cp.x = camDist;
				cp.y = 0;
				cp.z = 0;
				camera.up = new THREE.Vector3(0,1,0);
				break;
			case 2:
				cp.x = 0;
				cp.y = 0;
				cp.z = -camDist;
				camera.up = new THREE.Vector3(0,1,0);
				break;
			case 3:
				cp.x = -camDist;
				cp.y = 0;
				cp.z = 0;
				camera.up = new THREE.Vector3(0,1,0);
				break;
			case 4:
				cp.x = 0;
				cp.y = -camDist;
				cp.z = 0;
				camera.up = new THREE.Vector3(0,0,1);
				break;
			case 5:
				cp.x = 0;
				cp.y = camDist;
				cp.z = 0;
				camera.up = new THREE.Vector3(0,0,-1);
				break;
		}

		lastSlot = nextSlot;	
*/
	}
}



////////////////////////////////////////////////////////////////////////

// Rotate an object around an arbitrary axis in object space
var rotObjectMatrix;
function rotateAroundObjectAxis(object, axis, radians) {
    rotObjectMatrix = new THREE.Matrix4();
    rotObjectMatrix.makeRotationAxis(axis.normalize(), radians);

    object.matrix.multiply(rotObjectMatrix);

    object.rotation.setFromRotationMatrix(object.matrix);
}


var rotWorldMatrix;
// Rotate an object around an arbitrary axis in world space       
function rotateAroundWorldAxis(object, axis, radians) {
    rotWorldMatrix = new THREE.Matrix4();
    rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);

    rotWorldMatrix.multiply(object.matrix);                // pre-multiply

    object.matrix = rotWorldMatrix;

    object.rotation.setFromRotationMatrix(object.matrix);
}