'use strict';

// view for the game
function Board( scene , game ) {
	var _this = this;

	this.scene = scene;	// three js scene to draw into
	this.game = game;		// game logic

	var maxD = 60; // size in gl units of the entire board (roughly)
	this.dim = maxD/this.game.dim;	// width of the grid squares in gl units
  this.gut = this.dim/20;	// width of the gutter between grid squares in gl units

	this.status = "";
	this.toPlay = 1;
	this.gameOver = false;
	this.board = [];
	this.tokens = [];

	var interGeo;

	// update the board based on the game array
	// call after every addition to the game.board 
	this.update = function() {
		var x,y,z;

		for (x = 0; x < game.board.length; x++ ) {
			for (y = 0; y < game.board.length; y++ ) {
				for (z = 0; z < game.board.length; z++ ) {

					var gbVal = game.board[x][y][z];

					if (gbVal > 0) {

						if (_this.board[x][y][z].token == null) {

							var mesh = new THREE.Mesh( _this.tokens[gbVal].geometry, _this.tokens[gbVal].material );
							var offset = ( (_this.game.dim-1) / 2 ) * _this.dim;

							mesh.position.x = _this.dim*x-offset;
							mesh.position.y = _this.dim*y-offset;
							mesh.position.z = _this.dim*z-offset;

							mesh.name = x + "," + y + "," + z;

							_this.board[x][y][z].token = mesh;
							if (gbVal == 1)
								_this.board[x][y][z].spin = Math.random() * (0.02 - 0.01) + 0.01;
							else
								_this.board[x][y][z].spin = -1 * (Math.random() * (0.02 - 0.01) + 0.01);

							_this.scene.add(mesh);

						}

					}

				} // z
			} // y
		} // x
	};

// internals
	var makeTokenGeo = function() {
		// make the tokens
		// note: tokens[0] is intentionally left blank for the blank cells

		// player 1 box token
		_this.tokens[1] = {};
		_this.tokens[1].geometry = new THREE.BoxGeometry( _this.dim/2, _this.dim/2, _this.dim/2 ); 	
		_this.tokens[1].material = new THREE.MeshPhongMaterial( { color: 0xff9999 } ); 	

		// player 2 octohedron
		_this.tokens[2] = {};
		_this.tokens[2].geometry = new THREE.DodecahedronGeometry( _this.dim/3 );
		_this.tokens[2].material = new THREE.MeshPhongMaterial( {
			color: 0x99ff99,
			shading: THREE.FlatShading 
		});
	};

	var makeInteractiveGeo = function() {
		// make the invisible meshes for click tests
		var g = _this.gut/2;
		var d = _this.dim/2 - g;

		interGeo = new THREE.Geometry();

		interGeo.vertices.push(
			new THREE.Vector3( -d,  d, -d ), 	// 0
			new THREE.Vector3( -d, -d, -d ),	// 1  0 ________ 3
			new THREE.Vector3(  d, -d, -d ), 	// 2   /|     / |
			new THREE.Vector3(  d,  d, -d ),	// 3 4/_|____/  |
			new THREE.Vector3( -d,  d,  d ),	// 4  | |   7|  |
			new THREE.Vector3( -d, -d,  d ),	// 5  |1|____|__|2
			new THREE.Vector3(  d, -d,  d ),	// 6  | /    | /
			new THREE.Vector3(  d,  d,  d )		// 7  |/_____|/
																				//   5        6
		);

		interGeo.faces.push( 
			new THREE.Face3( 0, 1, 2 ),
			new THREE.Face3( 0, 2, 3 ),
			new THREE.Face3( 0, 4, 1 ),
			new THREE.Face3( 1, 4, 5 ),
			new THREE.Face3( 1, 5, 6 ),
			new THREE.Face3( 1, 6, 2 ),
			new THREE.Face3( 0, 3, 7 ),
			new THREE.Face3( 0, 7, 4 ),
			new THREE.Face3( 2, 6, 3 ),
			new THREE.Face3( 3, 6, 7 ),
			new THREE.Face3( 4, 7, 5 ),
			new THREE.Face3( 5, 7, 6 )
		);
		interGeo.computeFaceNormals();
		interGeo.computeVertexNormals();
	};

	var buildGameBoard = function() {

		// dims for the game board
		var bd = _this.game.dim; 	// number of squares per side
		var d = _this.dim;				// size of each square in gius 
		var x,y,z;
		var px,py,pz;
		var offset;
		var rr;

		// bars for the 3d grid
		var boardMat = new THREE.MeshPhongMaterial({
			color: 0xaaaaaa
		});

		var boardGeo = new THREE.BoxGeometry( _this.gut, _this.gut, d*bd );

		// add the grid
		offset = d*(bd/2-1);
		rr = Math.PI/2;

		for (x = 0; x < bd-1 ; x++) {
			for (y = 0; y < bd-1 ; y++) {
				for (z=0 ; z < bd-1 ; z++) {
					px = x*d-offset;
					py = y*d-offset;
					pz = z*d-offset;

					// and the game bars
					addStaticMesh( boardGeo, boardMat,   0, py, pz,  0, rr, 0); // x
					addStaticMesh( boardGeo, boardMat,  px,  0, pz, rr,  0, 0); // y
					addStaticMesh( boardGeo, boardMat,  px, py,  0,  0,  0, 0); // z
				}
			}
		}

		// add the interactive boxes
		
		offset = d*(bd/2-0.5); // 
		var interMat;
		_this.board = [];

		for (x = 0; x < bd ; x++) {
			_this.board[x] = [];
			for (y = 0; y < bd ; y++) {
				_this.board[x][y] = []; 
				for (z=0 ; z < bd ; z++) {
					
					px = x*d-offset;
					py = y*d-offset;
					pz = z*d-offset;
					_this.board[x][y][z] = {};

					if ( x == 0 || y == 0 || z == 0 || x == bd-1 || y == bd-1 || z == bd-1) {
						interMat = new THREE.MeshPhongMaterial({
							shading: THREE.FlatShading,
							transparent: true,
							opacity: 0.0,
							color: 0xffaaff
						});

						_this.board[x][y][z].interMesh = addStaticMesh(  interGeo, interMat, px,py,pz, 0,0,0);
						_this.board[x][y][z].interMesh.name = (x+","+y+","+z); 

					}
				}
			}
		}

		// add center block
		var geo = new THREE.BoxGeometry( d*(bd-2), d*(bd-2), d*(bd-2) );
		var mat = new THREE.MeshPhongMaterial( { color: 0x999999 } );
		addStaticMesh( geo,mat,0,0,0,0,0,0 );
	};

	// add given geo to the game board
	// for static meshes
	var addStaticMesh = function(geo, mat, px,py,pz, rx,ry,rz) {
		var mesh = new THREE.Mesh( geo, mat ); 
		
		mesh.position.x = px;
		mesh.position.y = py;
		mesh.position.z = pz;
		
		mesh.rotation.x = rx;
		mesh.rotation.y = ry;
		mesh.rotation.z = rz;
		
		mesh.updateMatrix();
		mesh.matrixAutoUpdate = false;
		_this.scene.add( mesh );
		return mesh;
	};

	makeTokenGeo();
	makeInteractiveGeo();
	buildGameBoard();
}	// Board
