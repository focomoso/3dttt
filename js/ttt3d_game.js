'use strict';

// model for the game and game logic
function Game( dim ) {
	var _this = this;

	this.dim = dim || 3; // size of board in squares (3x3x3, 4x4x4)
	this.board = [];
	this.toPlay = 1;

	this.resetBoard = function( dim ) {
		_this.dim = dim || _this.dim;
		if (_this.dim < 2) _this.dim = 2;

		var d = _this.dim;	// number of squares per side of the board (3x3x3, 4x4x4, ...)
		var b = [];					// new game board

		// build the array
		for (var x=0 ; x<d ; x++ ) {
			
			b[x] = [];
			
			for (var y=0 ; y<d ; y++ ) {
				
				b[x][y] = [];
				
				for (var z=0 ; z<d ; z++) {

					b[x][y][z] = 0; // 0 for empty, 1 for player one's token, 2 for player 2
					
				} //z
			} //y
		} //x
		_this.board = b;
	};

	this.playSlot = function(x,y,z) {
		_this.board[x][y][z] = _this.toPlay;
		_this.toPlay == 1 ? _this.toPlay = 2 : _this.toPlay = 1;
	};

	_this.resetBoard();

} // Game
